const Discord = module.require("discord.js");

module.exports.run = async (client, message, args) => {

    const m = await message.channel.send("Ping?")
    m.edit(`Pong! Delay: ${m.createdTimestamp - message.createdTimestamp}ms.`);

    return;
};

module.exports.help = {
    name: "ping"
}