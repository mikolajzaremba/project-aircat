const Discord = module.require("discord.js");

let symbols = {
    "0": ":zero:",
    "1": ":one:",
    "2": ":two:",
    "3": ":three:",
    "4": ":four:",
    "5": ":five:",
    "6": ":six:",
    "7": ":seven:",
    "8": ":eight:",
    "9": ":nine:",
    "<": ":arrow_backward:",
    ">": ":arrow_forward:",
    "!": ":exclamation:",
    "?": ":question:",
    "^": ":arrow_up_small:",
    "+": ":heavy_plus_sign:",
    "-": ":heavy_minus_sign:",
    "÷": ":heavy_division_sign:",
    ".": ":radio_button:"
}

module.exports.run = async (client, message, args) => {

    var emoji = args.join(" ").toLowerCase();
    let output = "";

    for (c = 0; c < emoji.length; c++) {
        if (/\s/g.test(emoji[c])) {
            output += "   ";
        } else if (/[abcdefghijklmnopqrstuvwxyz]/g.test(emoji[c])) {
            output += emoji[c].replace(emoji[c], " :regional_indicator_" + emoji[c] + ":");
        } else if (Object.keys(symbols).indexOf(emoji[c]) > -1) {
            output += emoji[c].replace(emoji[c], " " + symbols[emoji[c]]);
        } else {
            output += " " + emoji[c] + " "
        }

    }
    message.channel.send(output)

};

module.exports.help = {
    name: "emojify"
}