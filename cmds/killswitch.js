const Discord = module.require("discord.js");
const config = module.require("../config/config.json");
module.exports.run = async (client, message, args) => {

    let parameters = args.join(" ");

    let kserr = new Discord.RichEmbed()
        .setFooter(config.name, config.logo)
        .setTitle(config.accessDenied[0])
        .setDescription(`**${message.author.username}#${message.author.discriminator}** ${config.accessDenied[1]}`)
        .setThumbnail(config.iconAccess)
        .setColor("#ff4c4c")

    let kswarn = new Discord.RichEmbed()
        .setTitle('Warning!')
        .setDescription(`This command will shutdown the node.js server\nIf you really want to continue use \`>killswitch -confirm\` \n\n**Use only in case of emergency!**`)
        .setThumbnail(config.iconInfo)
        .setColor("#f4e842")
        .setFooter(config.name, config.logo)

    let shutdown = new Discord.RichEmbed()
        .setTitle(`${config.name} is going to be emergency shutted down!`)
        .setDescription(`Kill switch has been activated by **${message.author.username}#${message.author.discriminator}**\n\n Shutting down...`)
        .setThumbnail(config.iconInfo)
        .setColor("#4c96ff")
        .setFooter(config.name, config.logo)

    if (message.author.id !== config.botAdminId) return message.channel.send(kserr)
    else if (parameters == "-confirm") return killBot()
    else return message.channel.send(kswarn);

    function killBot() {
        console.log(`[INFO] ${message.author.username}#${message.author.discriminator} activated kill switch!`)
        console.log(`Shutting down...`)
        message.channel.send(shutdown)
        .then(msg => client.destroy())
    }
};

module.exports.help = {
    name: "killswitch"
}