const Discord = module.require("discord.js");
const config = module.require("../config/config.json");
module.exports.run = async (client, message, args, token) => {

    let parameters = args.join(" ");

    let kserr = new Discord.RichEmbed()
        .setFooter(config.name, config.logo)
        .setTitle(config.accessDenied[0])
        .setDescription(`**${message.author.username}#${message.author.discriminator}** ${config.accessDenied[1]}`)
        .setThumbnail(config.iconAccess)
        .setColor("#ff4c4c")

    let kswarn = new Discord.RichEmbed()
        .setTitle('Warning!')
        .setDescription(`This command will destroy ${config.name} client and try to login again\nIf you really want to continue use \`>restart -confirm\``)
        .setThumbnail(config.iconWarning)
        .setColor("#f4e842")
        .setFooter(config.name, config.logo)

    let shutdown = new Discord.RichEmbed()
        .setTitle(`${config.name} is restarting!`)
        .setDescription(`**${message.author.username}#${message.author.discriminator}** activated secure restart process of ${config.name}'s client\n\n Restarting...`)
        .setThumbnail(config.iconInfo)
        .setColor("#4c96ff")
        .setFooter(config.name, config.logo)

    let logged = new Discord.RichEmbed()
        .setTitle(`${config.name} has been successfully restarted!`)
        .setDescription(`Successfully ended process of secure restart activated by **${message.author.username}#${message.author.discriminator}**!`)
        .setThumbnail(config.iconSuccess)
        .setColor("#42f489")
        .setFooter(config.name, config.logo)

    if (message.author.id !== config.botAdminId) return message.channel.send(kserr)
    else if (parameters == "-confirm") return killBot()
    else return message.channel.send(kswarn);

    function killBot() {
        console.log(`[INFO] ${message.author.username}#${message.author.discriminator} activated secure restart of client!`)
        console.log(`Shutting down...`)
        message.channel.send(shutdown)
            .then(msg => client.destroy())
            .then(() => {
                try { // If auth.json exist, login in devMode using devToken

                    var token = require("../config/auth.json").token;
                    client.login(token)
                    console.log("[INFO] Developer Mode Enabled \n[INFO] Using DevToken to login into Discord API");
                } catch (ex) { // If not, use Mastertoken in Env.Variable to login in masterMode
                    var token = process.env.token;
                    client.login(token)
                    console.log("[INFO] Production Mode Enabled \n[INFO] Using MasterToken to login into Discord API ")
                }

                setTimeout(restartnotify, 10000)

                function restartnotify() {
                    console.log("[INFO] Successfully restarted!")
                    message.channel.send(logged)
                }
            });


    }
};

module.exports.help = {
    name: "restart"
}