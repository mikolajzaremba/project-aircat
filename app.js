const config = require("./config/config.json");
console.log(`\n${config.name}\n[INFO] Starting...`)
const Discord = require("discord.js");
const fs = require("fs");
const client = new Discord.Client({autoReconnect:true});
client.commands = new Discord.Collection();


try { // If auth.json exist, login in devMode using devToken
    var token = require("./config/auth.json").token;
    var prefix = config.devPrefix;
    var mode = "devmode";
    console.log("[INFO] Developer Mode Enabled \n[INFO] Using DevToken to login into Discord API");
} catch (ex) { // If not, use Mastertoken in Env.Variable to login in masterMode
    var token = process.env.token;
    var prefix = config.prefix;
    var mode = "mastermode";
    console.log("[INFO] Production Mode Enabled \n[INFO] Using MasterToken to login into Discord API ")
}

console.log('[INFO] Looking for commands in /cmds directory...');
fs.readdir("./cmds", (err, files) => {
    if (err) console.error("[ERROR]", err);

    let jsfiles = files.filter(f => f.split(".").pop() === "js");
    if (jsfiles.length <= 0) {
        console.log("[WARN] No commands found!");
        return;
    }

    console.log(`[INFO] Loading ${jsfiles.length} commands...`);

    jsfiles.forEach((f, i) => {
        let props = require(`./cmds/${f}`);
        console.log(`   [+] ${i + 1}: ${f} loaded`);
        client.commands.set(props.help.name, props);
    });
});

client.on("ready", async () => {
    console.log(`\n[INFO] Logged in to Discord API as ${client.user.tag}`);
    try {
        let link = await client.generateInvite(["ADMINISTRATOR"]);
        console.log(`[INFO] oAuth2 invite link: ${link}`);
    } catch (e) {
        console.log(e.stack);
    }
    console.log(`[INFO] ${config.name} is online and ready! \n`);
    

})

client.on("message", async (message, config, token)=> {
    if (message.author.bot) return;
    if (message.channel.type === "dm") return;

    let messageArray = message.content.split(" ");
    let command = messageArray[0];
    let args = messageArray.slice(1);

    if (!command.startsWith(prefix)) return;

    let cmd = client.commands.get(command.slice(prefix.length))
    if (cmd) cmd.run(client, message, args, token, config);


});

client.on("disconnected", function () {

    console.log("[ERROR] Disconnected!");
    process.exit(1); 

});

client.login(token);